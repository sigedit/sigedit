
add_executable(
    signal_test
    
    ${SIGNAL_TEST_DIR}/main.cpp
)

target_link_libraries(
    signal_test
    
    siglib
)
