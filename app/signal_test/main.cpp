

#include <iostream>
#include "Signal.hpp"

int main ( int argc, char ** argv )
{
    double a_vals[] = {1.0,2.0,3.0};
    double b_vals[] = {5.0,8.0,13.0};
    Signal a ( a_vals, 3 );
    Signal b ( b_vals, 3);
    
    Signal c = a+b;
    
    size_t i = 0,
    i_max = c.size();
    while(i<i_max){
        std::cout<<"c["<<i<<"] = "<<c[i]<<"\n";
        i++;
    }
    
    std::cout<<to_string(c);
    return 0;
}


// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
