
#include "Signal.hpp"

Signal::Signal()
{

}
Signal::Signal ( const Signal& sig )
{
    size_t len = sig.size();
    _sig.resize ( len );
    size_t i = 0;
    while ( i<len )
    {
        _sig[i] = sig[i];
        i++;
    }
}


Signal::Signal ( const double * sig, const size_t len )
{
    if ( sig==nullptr )
    {
        return;
    }
    _sig.resize ( len );
    size_t i = 0;
    while ( i<len )
    {
        _sig[i] = sig[i];
        i++;
    }
}

Signal& Signal::operator= ( const Signal& right )
{
    if ( this == &right )
    {
        return *this;
    }
    size_t len = right.size();
    _sig.resize ( len );
    size_t i = 0;
    while ( i<len )
    {
        _sig[i] = right[i];
        i++;
    }
    return *this;
}
Signal Signal::operator+ ( const Signal& right )
{
    const Signal& left = *this;
    Signal ret;
    size_t len = std::max ( left.size(), right.size() );
    size_t i = 0;
    while ( i<len )
    {
        ret[i] = left[i] + right[i];
        i++;
    }

    return ret;
}
Signal Signal::operator- ( const Signal& right )
{
    const Signal& left = *this;
    Signal ret;
    size_t len = std::max ( left.size(), right.size() );
    size_t i = 0;
    while ( i<len )
    {
        ret[i] = left[i] - right[i];
        i++;
    }

    return ret;
}

Signal &  Signal::operator += ( const Signal & right )
{
    const Signal& left = *this;
    //Signal ret;
    size_t len = std::max ( left.size(), right.size() );
    size_t i = 0;
    while ( i<len )
    {
        _sig[i] += right[i];
        i++;
    }
    return *this;
}

Signal &  Signal::operator -= ( const Signal & right )
{
    const Signal& left = *this;
    //Signal ret;
    size_t len = std::max ( left.size(), right.size() );
    size_t i = 0;
    while ( i<len )
    {
        _sig[i] -= right[i];
        i++;
    }
    return *this;
}

double& Signal::operator[] ( const size_t idx )
{
    if ( _sig.size() <=idx )
    {
        _sig.resize ( idx+1 );
    }
    return _sig[idx];
}
double Signal::operator[] ( const size_t idx ) const
{
    if ( _sig.size() <=idx )
    {
        return 0.0;
    }
    return _sig[idx];
}


size_t Signal::size() const
{
    return _sig.size();
}



std::string to_string ( const Signal& sig )
{
    std::string ret;

    size_t i = 0, i_max = sig._sig.size();

    while ( i<i_max )
    {
        ret += "[" + std::to_string ( i ) + "]: " + std::to_string ( sig._sig[i] ) + "\n";
        i++;
    }
    return ret;
}

// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
