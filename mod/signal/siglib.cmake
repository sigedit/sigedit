
include_directories(${SIGLIB_DIR})

add_library(
  siglib
  
  ${SIGLIB_DIR}/Signal.cpp
)



FIND_PACKAGE(Doxygen)
IF(DOXYGEN_FOUND)
    SET(DOXYGEN_INPUT ${SIGLIB_DIR})
    CONFIGURE_FILE(${SIGLIB_DIR}/doxygen.conf ${CMAKE_CURRENT_BINARY_DIR}/doxygen.conf)

    ADD_CUSTOM_TARGET(siglib_doc COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doxygen.conf)
    add_dependencies(siglib siglib_doc)
ELSE(DOXYGEN_FOUND)
    MESSAGE(WARNING "Doxygen not found - Reference manual will not be created")
ENDIF(DOXYGEN_FOUND)
