#pragma once

#include <vector>
#include <string>



/**
 * Сигнал
 * 
 * Класс, определяющий цифровой сигнал.
 * Сигнал задается точками, все точки пронумерованы.
 * 
 * Реализует базовые операции над сигналами:
 * - сложение
 * - вычитание
 * - умножение на число
 */
class Signal
{
private:

public:

    /**
     * Создает пустой сигнал
     */
    Signal();


    /**
     * Создать сигнал, являющийся копией другого
     * @param sig - исходный сигнал
     */
    Signal ( const Signal & sig );

    /**
     * Создать сигнал, являющийся копией другого
     * @param sig - исходный сигнал
     * @param len - Количество точек в исходном сигнале
     */
    Signal ( const double * sig, const size_t len );

    /**
     * Сделать сигнал копией другого
     * @param right - исходный сигнал
     * @return ссылка на получившийся сигнал
     */
    Signal & operator = ( const Signal & right );

    
    /**
     * Поэлементная сумма сигналов ( Cn = An + Bn )
     * @param right - Сигнал, с которым суммировать этот сигнал
     * @return Сумма этого и второго сигналов
     */
    Signal operator + ( const Signal & right );
    
    /**
     * Поэлементная разность сигналов ( Cn = An - Bn )
     * @param right - Сигнал, который вычитать из этого сигнала
     * @return Разность этого и второго сигналов
     */
    Signal operator - ( const Signal & right );

    /**
     * Поэлементная сумма сигналов
     * 
     * Эквивалентно записи A = A + B
     * @param right - Сигнал, с которым суммировать этот сигнал
     * @return Этот сигнал
     */
    Signal &  operator += ( const Signal & right );
    
    /**
     * Поэлементная разность сигналов
     * 
     * Эквивалентно записи A = A - B
     * @param right - Сигнал, который вычитать из этого сигнала
     * @return Этот сигнал
     */
    Signal &  operator -= ( const Signal & right );

    
    /**
     * Взять ссылку на значение сигнала в точке
     * @param idx - номер точки
     */
    double & operator[] ( const size_t idx );
    
    /**
     * Взять значение сигнала в точке
     * @param idx - номер точки
     */
    double operator[] ( const size_t idx ) const;

    /**
     * Преобразовать сигнал в строку вида "[i]: Si"
     * @param sig - сигнал, который нужно преобразовать
     * @return строку
     */
    friend std::string to_string ( const Signal& sig );

    
    /**
     * Получить максимальное значение сигнала
     * @return максимальное значение
     */
    double max();
    
    /**
     * Получить минимальное значение сигнала
     * @return минимальное значение
     */
    double min();
    
    /**
     * Получить номер первой точки с макс. значением сигнала
     * @return номер точки
     */
    size_t max_id();
    
    /**
     * Получить номер первой точки с мин. значением сигнала
     * @return номер точки
     */
    size_t min_id();

    /**
     * Получить количество точек
     */
    size_t size() const;
protected:
    /**
     * Значения сигнала в точках
     */
    std::vector<double> _sig;
};


//std::string to_string ( const Signal& sig );
// kate: indent-mode cstyle; indent-width 4; replace-tabs on; 
